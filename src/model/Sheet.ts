import { Composer } from "./Composer";
import { Location } from "./Location";

export interface Sheet {
    id: number;
    title: string;
    composer: Composer;
    location: Location;
}
