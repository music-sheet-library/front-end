import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Composer } from '../../model/Composer';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_COMPOSERS_QUERY, ADD_SHEET_MUTATION, GET_SHEETS_REPOSITORY } from '../../queries/queries';
import {makeStyles,  createStyles, Theme, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
        addButton: {
            margin: theme.spacing(2),
            width: 'auto'
        },
    }),
);

interface ComposerRepositoryData {
    composers: Composer[];
}

export default function AddSheetDialog() {
    const classes = useStyles({});
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const { loading, data } = useQuery<ComposerRepositoryData>(GET_COMPOSERS_QUERY);
    const [addSheet] = useMutation(ADD_SHEET_MUTATION);

    const [state, setState] = useState({ name: "", composerID: 0 })

    function displayComposers() {
        if (loading) {
            return (<option disabled>Lade Komponisten...</option>);
        } else if (data) {
            return data.composers.map(composer => {
                return (<MenuItem key={composer.id} value={composer.id}>{composer.name}</MenuItem>)
            })
        }
    };

    const submitForm = (e: React.FormEvent) => {
        e.preventDefault();
        console.log(state);
        addSheet({ variables: { title: state.name, composerId: state.composerID }, refetchQueries:[{query: GET_SHEETS_REPOSITORY}] })
        setOpen(false);
    }

    const handleChange = (event: React.ChangeEvent<{value: unknown}>) => {
        const newID = event.target.value as number;
        setState({ name: state.name, composerID: newID })
    }

    return (
        <>
            <Button variant="outlined" color="primary" onClick={handleClickOpen} className={classes.addButton}>
                Neue Noten hinzufügen
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Noten hinzufügen</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Bitte füllen Sie die nachfolgenden Felder aus, um einen neuen Eintrag zu erstellen.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="title"
                        label="Titel"
                        type="text"
                        onChange={(e) => setState({ name: e.target.value, composerID: state.composerID })}
                        fullWidth
                    />
                    <form className={classes.root} autoComplete="off">
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="age-simple">Komponist</InputLabel>
                            <Select
                                value={state.composerID}
                                onChange={handleChange}
                                inputProps={{
                                    name: 'composer',
                                    id: 'composer-simple',
                                }}
                            >
                                {displayComposers()}
                            </Select>
                        </FormControl>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Abbrechen
                    </Button>
                    <Button onClick={submitForm} color="primary">
                        Hinzufügen
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}
