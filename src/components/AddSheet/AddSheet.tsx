import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_COMPOSERS_QUERY, ADD_SHEET_MUTATION } from '../../queries/queries';
import { Composer } from '../../model/Composer';

interface ComposerRepositoryData {
    composers: Composer[];
}

const AddSheet = () => {
    const { loading, data } = useQuery<ComposerRepositoryData>(GET_COMPOSERS_QUERY);
    const [addSheet] = useMutation(ADD_SHEET_MUTATION);

    const [state, setState] = useState({ name: "", composerID: "" })

    function displayComposers() {
        if (loading) {
            return (<option disabled>Lade Komponisten...</option>);
        } else if (data) {
            return data.composers.map(composer => {
                return (<option key={composer.id} value={composer.id}>{composer.name}</option>)
            })
        }
    };

    const submitForm = (e: React.FormEvent) => {
        e.preventDefault();
        console.log(state);
        addSheet({ variables: { title: state.name, composerId: parseInt(state.composerID) } })
    }

    return (
        <form id="add-sheet" onSubmit={submitForm}>
            <div className="field">
                <label>Titel:</label>
                <input type="text" onChange={(e) => setState({ name: e.target.value, composerID: state.composerID })} />
            </div>

            <div className="field">
                <label>Komponist</label>
                <select onChange={(e) => setState({ name: state.name, composerID: e.target.value })}>
                    <option>Wähle Komponist</option>
                    {displayComposers()}
                </select>
            </div>

            <button>+</button>
        </form>
    )
}

export default AddSheet;