import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useQuery } from '@apollo/react-hooks';
import { Sheet } from '../../model/Sheet';
import { GET_SHEETS_REPOSITORY } from '../../queries/queries';
import { IconButton } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen } from '@fortawesome/free-solid-svg-icons'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            marginTop: theme.spacing(3),
            marginRight: theme.spacing(10),
            overflowX: 'auto',
        },
        table: {
            minWidth: 650
        },
    }),
);

interface SheetRepositoryData {
    sheets: Sheet[];
}

export default function SimpleTable() {
    const classes = useStyles({});
    const { loading, data } = useQuery<SheetRepositoryData>(GET_SHEETS_REPOSITORY);

    return (
        <Paper className={classes.root}>
            {
                loading ? (
                    <p>Lade Noten...</p>
                ) : (
                        <Table className={classes.table}>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ID</TableCell>
                                    <TableCell >Titel</TableCell>{/*possible with align="right" */}
                                    <TableCell >Komponist</TableCell>{/*possible with align="right" */}
                                    <TableCell>Ort</TableCell>
                                    {/*<TableCell></TableCell>*/}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data && data.sheets.map(sheet => (
                                    <TableRow key={sheet.id}>
                                        <TableCell component="th" scope="row">
                                            {sheet.id}
                                        </TableCell>
                                        <TableCell >{sheet.title}</TableCell>{/*possible with align="right" */}
                                        <TableCell >{sheet.composer.name}</TableCell>{/*possible with align="right" */}
                                        <TableCell>{sheet.location.name}</TableCell>
                                        <TableCell><IconButton><FontAwesomeIcon icon={faPen}/></IconButton></TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    )
            }

        </Paper>
    );
}