import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { Sheet } from '../../model/Sheet';
import { GET_SHEETS_REPOSITORY } from '../../queries/queries';

interface SheetRepositoryData {
    sheets: Sheet[];
}

const SheetList = (props: any) => {

    const {loading, data} = useQuery<SheetRepositoryData>(GET_SHEETS_REPOSITORY);
    
    return (
        <div>
            { loading? (
                <p>loading...</p>
            ):(                
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Komponist</th>
                        </tr>
                    </thead>
                    <tbody>
                        { data && data.sheets.map(sheet => (
                            <tr key={sheet.title}>
                                <td>{sheet.id}</td>
                                <td>{sheet.title}</td>
                                <td>{sheet.composer.name}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            )}
        </div>
    )
}

export default SheetList;