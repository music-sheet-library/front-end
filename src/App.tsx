import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { makeStyles, Theme, createStyles, Paper, Typography } from '@material-ui/core';
import './App.css';
import ButtonAppBar from './components/ButtonAppBar/ButtonAppBar';
import SheetTable from './components/SheetTable/SheetTable';
import AddSheetDialog from './components/AddSheet/AddSheetDialog';

const client = new ApolloClient({
  uri: 'http://localhost:8080/graphql',
});

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      //width: '100%',
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(5),
      marginRight: theme.spacing(5),
      overflowX: 'auto',
    },
    heading: {
      marginTop: theme.spacing(2),
      marginLeft: theme.spacing(2),
    },
    addButton: {

    },
  }),
);

const App: React.FC = () => {

  const classes = useStyles({});
  return (
    <ApolloProvider client={client}>
      <ButtonAppBar />
      <Paper className={classes.root}>
        <>
          <Typography variant="h3" component="h1" className={classes.heading}>
            Vorhandene Noten
          </Typography>
          <SheetTable />
          <AddSheetDialog/>
        </>
      </Paper>
    </ApolloProvider>
  );
}

export default App;
