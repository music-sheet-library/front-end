import {gql} from 'apollo-boost';

const GET_SHEETS_REPOSITORY = gql`
    {
        sheets {
            id,
            title,
            composer {
                id,
                name
            },
            location {
                id,
                name
            }
        }
    }
`

const GET_COMPOSERS_QUERY = gql`
    {
        composers {
            id
            name
        }
    }
`

const ADD_SHEET_MUTATION = gql`
    mutation($title: String!, $composerId: Int!) {
        createSheet(input: {
                title: $title
                composerId: $composerId
                locationId: 0
                sheetType: MASS
            }){
            id
            title
        }
    }
`

export { GET_SHEETS_REPOSITORY, GET_COMPOSERS_QUERY, ADD_SHEET_MUTATION};